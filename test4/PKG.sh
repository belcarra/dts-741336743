#!/bin/bash

FIG() {
        echo
        type figlet >/dev/null 2>/dev/null
        if [ $? -eq 0 ] ; then
                figlet -w 160 $* 1>&2
        else
                echo $* 1>&2
        fi
}


onerror() {
        set +x
        echo onerror: $*

        exit -1
}
killme() {
        set +x
        echo -en "\n\n$*\n\n" 1>&2
        FIG FAILED
        kill -1 $$
}

onexit() {
	set -x
	#[ -f ${NOTARIZE} ] && rm -vf ${NOTARIZE}
}

trap onerror 1 2 9 15
trap onexit 0


. ./.CERTS

echo CERT_DEVELOPER_ID_APPLICATION: $CERT_DEVELOPER_ID_APPLICATION: 
security  unlock-keychain -p '{}' /Users/${USER}/Library/Keychains/login.keychain 

PRODUCT_NAME=`make PRODUCT_NAME`
CONFIGURATION=`make CONFIGURATION`

FIG Packaging ${PRODUCT_NAME}

pushd build
rm -f ${PRODUCT_NAME}.pkg


FIG Entitlements ${PRODUCT_NAME}
( set -x; codesign -d --entitlements :- ${CONFIGURATION}/${PRODUCT_NAME}.app )

for i in ${CONFIGURATION}/${PRODUCT_NAME}.app/Contents/Library/SystemExtensions/* ; do
    (set -x; codesign -d --entitlements :- $i )
done

FIG ProvisionFiles ${PRODUCT_NAME}
( set -x; security cms -D -i ${CONFIGURATION}/${PRODUCT_NAME}.app/Contents/embedded.provisionprofile )

for i in ${CONFIGURATION}/${PRODUCT_NAME}.app/Contents/Library/SystemExtensions/* ; do
    ( set -x; security cms -D -i $i/embedded.provisionprofile )
done

FIG Codesign verify ${PRODUCT_NAME}
(set -x; codesign --verify -vvv --deep --strict ${CONFIGURATION}/${PRODUCT_NAME}.app)
for i in ${CONFIGURATION}/${PRODUCT_NAME}.app/Contents/Library/SystemExtensions/* ; do
    (set -x; codesign --verify -vvv --deep --strict $i )
done

FIG Assessing ${PRODUCT_NAME}

(set -x ; spctl -vvv --assess --type exec ${CONFIGURATION}/${PRODUCT_NAME}.app || killme spctl --assess --type exec failed )


FIG productbuild ${PRODUCT_NAME}

( set -x; productbuild \
    --sign $CERT_DEVELOPER_ID_INSTALLER \
    --component ${CONFIGURATION}/${PRODUCT_NAME}.app /Applications/ ${PRODUCT_NAME}.pkg ||
    killme productbuild failed
)

FIG  Check ${PRODUCT_NAME}.pkg signature
( set -x; pkgutil --check-signature ${PRODUCT_NAME}.pkg || killme pkgutil --check-signature failed)

FIG  Asses ${PRODUCT_NAME}.pkg 
( set -x; spctl -vvv --assess --type install ${PRODUCT_NAME}.pkg || killme spctl --assess failed)

../NOTARIZE ${PRODUCT_NAME}.pkg

exit

codesign \
    --sign $CERT_DEVELOPER_ID_PRODUCTNAME \
    --options runtime \
    --verbose \
    --force \
    --deep \
    ${PRODUCT_NAME}.pkg

codesign --verify --verbose ${PRODUCT_NAME}.pkg

exit
../NOTARIZE ${PRODUCT_NAME}.pkg

exit



