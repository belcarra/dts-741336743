#!/bin/bash

INFO=$(mktemp /tmp/INFO-XXXXXXX)


xcrun altool --notarization-info $1 --username sl@belcarra.com --password ovaa-vmiq-bras-kawm > ${INFO}

sed -e 's/\(LogFileURL: \).*/\1 **REMOVED**/' < ${INFO}

LOGFILEURL=`xcrun altool --notarization-info $1 --username sl@belcarra.com --password ovaa-vmiq-bras-kawm | sed -n -e 's/.*LogFileURL: //p'`

echo 
echo LogFileURL contents

curl $LOGFILEURL

