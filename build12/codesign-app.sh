#!/bin/bash
#
# N.B. this script is designed to work correctly if run in ssh terminal session,
# but only if the certificate is first authenticated and allowed by doing
# at least one build and signing session from a terminal on the console. That
# will allow a security dialog to open for your password and you can then
# click on "Allow always".
#
# If you do not do the above you will see:
#   build/Debug/test-simple.app: replacing existing signature
#   build/Debug/test-simple.app: errSecInternalComponent
#


if [ $# -ne 2 ]; then 
	echo Usage ./codesign-app.sh customerprefix 
	exit 1
fi

CONFIGURATION=${1:=}
PREFIX=${2:=}
PRODUCT_NAME=$(make PREFIX=${PREFIX} PRODUCT_NAME)
#PROVISIONPROFILE=$(make PREFIX=${PREFIX} PROVISIONPROFILE)
ENTITLEMENTS=$(make PREFIX=${PREFIX} ENTITLEMENTS)

echo CONFIGURATION: $CONFIGURATION
echo PREFIX: $PREFIX
echo PRODUCT_NAME: $PRODUCT_NAME
echo PROVISIONPROFILE: $PROVISIONPROFILE
echo ENTITLEMENTS: $ENTITLEMENTS

# Replace with your identity
# Developer ID Application
#readonly CODE_SIGN_IDENTITY=AF81FE1962517ECAABF73719286C4829C4C06EA5

# 3rd party Mac Developer Application
#readonly CODE_SIGN_IDENTITY=24245EA9D78B45D39F5B8ABD20ED826DB40AC159

#readonly CODE_SIGN_IDENTITY=CE8A91E49650C96DD821A8EBF3BA441BD0EA3329

. ./.CERTS

security  unlock-keychain -p '{}' /Users/${USER}/Library/Keychains/login.keychain 

set -e # forbid command failure
set -x

# copy provisioning file
#set -x
#cp ${PROVISIONPROFILE} build/${CONFIGURATION}/${PRODUCT_NAME}.app/Contents


# N.B. --deep flag is needed to prevent this error if we are copying provisionfile into bundle
#                   
#           build/Debug/test-simple.app: replacing existing signature
#           build/Debug/test-simple.app: code object is not signed at all

codesign \
    --sign $CERT_DEVELOPER_ID_APPLICATION \
    --entitlements ${ENTITLEMENTS} \
    --options runtime \
    --verbose \
    --force \
    --timestamp \
    build/${CONFIGURATION}/${PRODUCT_NAME}.app

codesign --verify --verbose \
    build/${CONFIGURATION}/${PRODUCT_NAME}.app

exit

