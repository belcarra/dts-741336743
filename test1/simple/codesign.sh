#!/bin/bash


set -x

security  unlock-keychain -p '{}' /Users/${USER}/Library/Keychains/login.keychain 
/usr/bin/codesign \
    --force \
    --sign AF81FE1962517ECAABF73719286C4829C4C06EA5 \
    --entitlements entitlements.plist \
    --timestamp \
    /Users/sl/work20/musbland/simple-app/build/Debug/test-simple.app
    
