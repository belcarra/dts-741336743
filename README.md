#DTS 741336743
## Tue 21 Jul 2020 23:00:56 PDT
# stuart.lynne@belcarra.com
# 604-518-1749

## PLATFORM AND VERSION
### OS X
### Mac mini (Late 2014) running Big Sur for testing, Mac mini (2018) for development.

## DESCRIPTION OF PROBLEM
Belcarra is developing a Network over USB DriverKit extension and an installation program for the DriverKit extension.

I have the application and driverkit extension working when run on a Big Sur test system with SIP disabled. I.e. the extension loads, USB probe works, network interface create and works. USB connect / disconnect all work.


I am attempting to create a .pkg with product build and then notarize that.

Currently (as of 2020-07-22) my PKG and NOTARIZE script work to get me a notarized pkg that will install on my test system.

That installs, runs and attempts device extension activation, with the system dialog opening, and requiring unlock in the security pane etc.

But after clicking allow, while I see that the driver is activated and enabled, plugging the USB test device in fails to get the driver loaded.

```
+ systemextensionsctl list
1 extension(s)
--- com.apple.system_extension.driver_extension
enabled    active    teamID    bundleID (version)    name    [state]
*    *    F7XQY5N68B    com.belcarra.driverkit (1.0/1)    com.belcarra.driverkit    [activated enabled]
```

Device enumeration

```
2020-07-22 23:09:24.008 A  ReportCrash[245:5f7] (CFOpenDirectory) Retrieve node details
2020-07-22 23:09:24.008 Df icdd[578:1170] [com.apple.imagecapture:icdd]       =>  [Matching] | [ 0x02,0x0a,0x00 ]
2020-07-22 23:09:24.008 Df icdd[578:1170] [com.apple.imagecapture:icdd]             Inferior | 0x1000000a - [USB][ CDC ECM-Subset 15ec ] ( 2, a, 0) @ 0x14700000 |
```

Crash report, file is in archive.

```
2020-07-22 23:09:25.040 Df ContextService[748:17aa] [com.apple.siri.context.service:LuceneEngine] Using bloom filter: (null)
2020-07-22 23:09:25.122 A  suggestd[672:164f] (ContextKit) ContextRequest capabilitiesWithReply
2020-07-22 23:09:25.275 Df ReportCrash[245:5f7] (CrashReporterSupport) Saved crash report for com.belcarra.driverkit[722] version ??? to com.belcarra.driverkit_2020-07-22-230925_mmtest.crash
```
From the crash report:

```
System Integrity Protection: enabled

Crashed Thread:        Unknown

Exception Type:        EXC_CRASH (Code Signature Invalid)
Exception Codes:       0x0000000000000000, 0x0000000000000000
Exception Note:        EXC_CORPSE_NOTIFY

Termination Reason:    Namespace CODESIGNING, Code 0x1
```

Disabling *SIP* on the test system, with the driverkit extension installed and activated, after rebooting, USB works correctly to load the driver and it
functions correctly.


## Summary

The install program and driverkit extension work correctly, at least with SIP disabled.

The install program runs with SIP enabled indicating that it is signed appropriately. 

PKG and NOTARIZE work to get a notarized .pkg file.

The pkg file successfully installs.

The resulting app runs and attempts to activate.

After unlocking in security pane, and clicking allow, and plugging in the USB test device, we see errors in the log file, a crash report is generated and there
is no evidence of the driver starting.

The crash report indicates a codesigning issue.

Disabling SIP results in working driver extension.


## STEPS TO REPRODUCE

This archive has two directories:

- simple - a sample build directory, with codesign, pkg and notarize scripts
- driverkit - from the driverkit project, entitlements and Info.plist 

## 


