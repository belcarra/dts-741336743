#!/bin/bash
# N.B. this script is designed to work correctly if run in ssh terminal session,
# but only if the certificate is first authenticated and allowed by doing
# at least one build and signing session from a terminal on the console. That
# will allow a security dialog to open for your password and you can then
# click on "Allow always".
#
# If you do not do the above you will see:
#   build/Debug/test-simple.app: replacing existing signature
#   build/Debug/test-simple.app: errSecInternalComponent
#

if [ $# -ne 2 ]; then 
	echo Usage ./codesign-dext.sh configuration customerprefix
	exit 1
fi

CONFIGURATION=${1:=}
PREFIX=${2:=}
PROJECT_BUNDLE_IDENTIFIER=$(make PREFIX=${PREFIX} PROJECT_BUNDLE_IDENTIFIER)
ENTITLEMENTS=$(make PREFIX=${PREFIX} ENTITLEMENTS)
PROVISIONPROFILE=$(make PREFIX=${PREFIX} PROVISIONPROFILE)

echo CONFIGURATION: $CONFIGURATION
echo PREFIX: $PREFIX
echo PROJECT_BUNDLE_IDENTIFIER: $PROJECT_BUNDLE_IDENTIFIER
echo ENTITLEMENTS: $ENTITLEMENTS
echo PROVISIONPROFILE: $PROVISIONPROFILE



# Use this to find your certificate identity:
#	security find-identity
#
#readonly CODE_SIGN_IDENTITY=AF81FE1962517ECAABF73719286C4829C4C06EA5

. ../.CERTS

#eval $(sed -e '/\/\//d' < USBApp.xcconfig)

set -e

security  unlock-keychain -p '{}' /Users/${USER}/Library/Keychains/login.keychain 

set -x

# copy provisioning file
set -x
#cp ${PROVISIONPROFILE} build/${CONFIGURATION}-driverkit/${PROJECT_BUNDLE_IDENTIFIER}.dext/

codesign \
    --sign $CERT_DEVELOPER_ID_APPLICATION \
    --entitlements ${ENTITLEMENTS} \
    --options runtime \
    --verbose \
    --force \
    --timestamp \
    build/${CONFIGURATION}-driverkit/${PROJECT_BUNDLE_IDENTIFIER}.dext

codesign --verify --verbose \
    build/${CONFIGURATION}-driverkit/${PROJECT_BUNDLE_IDENTIFIER}.dext

