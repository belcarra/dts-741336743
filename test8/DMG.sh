#!/bin/bash

FIG() {
        echo
        type figlet >/dev/null 2>/dev/null
        if [ $? -eq 0 ] ; then
                figlet -w 160 $* 1>&2
        else
                echo $* 1>&2
        fi
}


onerror() {
        set +x
        echo onerror: $*

        exit -1
}
killme() {
        set +x
        echo -en "\n\n$*\n\n" 1>&2
        FIG FAILED
        kill -1 $$
}

onexit() {
	set -x
	#[ -f ${NOTARIZE} ] && rm -vf ${NOTARIZE}
}

trap onerror 1 2 9 15
trap onexit 0

. ./.CERTS

echo CERT_DEVELOPER_ID_APPLICATION: $CERT_DEVELOPER_ID_APPLICATION: 
echo CERT_DEVELOPER_ID_INSTALLER: $CERT_DEVELOPER_ID_INSTALLER: 

security  unlock-keychain -p '{}' /Users/${USER}/Library/Keychains/login.keychain 

PRODUCT_NAME=`make PRODUCT_NAME`

#PRODUCT_NAME=Karabiner-DriverKit-ExtensionManager
CONFIGURATION=`make CONFIGURATION`

FIG Packaging ${PRODUCT_NAME}

pushd build

rm -f ${PRODUCT_NAME}.dmg
mkdir -p mount
( set -x
hdiutil create -size 4m -fs HFS+ -volname ${PRODUCT_NAME} ${PRODUCT_NAME}.dmg
hdiutil attach ${PRODUCT_NAME}.dmg -mountpoint mount
find mount
cp -va Debug/${PRODUCT_NAME}.app mount/
pushd mount
#ln -s /Applications @
#ls -ltR
rm -rf .fs*
find
popd
hdiutil detach mount
xattr -cr ${PRODUCT_NAME}.dmg
)

(set -x
#codesign --force --timestamp --sign ${CERT_DEVELOPER_ID_INSTALLER} ${PRODUCT_NAME}.dmg || killme codesign failed
codesign --force --timestamp --sign ${CERT_DEVELOPER_ID_APPLICATION} ${PRODUCT_NAME}.dmg || killme codesign failed
)

FIG  Asses ${PRODUCT_NAME}.pkg 
( set -x; spctl -vvv --assess --type install ${PRODUCT_NAME}.dmg || killme spctl --assess failed)

spctl -a -t open --context context:primary-signature -vv ${PRODUCT_NAME}.dmg

../NOTARIZE ${PRODUCT_NAME}.dmg



